Credits
=======



Code
----

The source code is generously hosted by `GitLab <https://gitlab.com>`_.

Goodvibes is open-source and simply couldn't exist without the various
open-source libraries it relies on. Here's a non-exhaustive list of projects
that make it possible.

 * `GLib <https://wiki.gnome.org/Projects/GLib>`_
 * `GStreamer <https://gstreamer.freedesktop.org>`_
 * `GTK+ <https://www.gtk.org>`_
 * `LibSoup <https://wiki.gnome.org/Projects/libsoup>`_

There are many open-source audio players around, and having all this code
available made it much easier to design and write Goodvibes. Thanks to all of
them for sharing their code.

 * `Rhythmbox <https://wiki.gnome.org/Apps/Rhythmbox>`_
 * `Totem <https://wiki.gnome.org/Apps/Totem>`_
 * `Quodlibet <https://quodlibet.readthedocs.io>`_
 * `Parole <http://docs.xfce.org/apps/parole/introduction>`_
 * `RadioTray <http://radiotray.sourceforge.net>`_

And at last, a personal thank you to *mgo* and *jcdr*, two fellow engineers who
taught me the job. Hope you guys are doing well :)



Translation
-----------

The translation effort is generously hosted by `Weblate <https://weblate.org>`_.

Here are the people who translate Goodvibes at the moment:

 * Lukáš Linhart - Czech (cs)
 * Michal Čihař - Czech (cs)
 * Andreas Kleinert - German (de)
 * Vinz - German (de)
 * Daniel Bageac - English (United States) (en_US)
 * Benages - Spanish (es)
 * Holman Calderón - Spanish (es)
 * Étienne Deparis - French (fr)
 * Notramo -  (hu)
 * Allan Nordhøy - Norwegian Bokmål (nb_NO)
 * Heimen Stoffels - Dutch (nl)
 * Manuela Silva - Portuguese (Portugal) (pt_PT)
 * Алексей Выскубов - Russian (ru)



Artwork
-------

`Hector Lahminèwskï <http://lahminewski-lab.net>`_ is the author of the artwork.



Documentation
-------------

This documentation is generously hosted by `Read the Docs <https://readthedocs.org>`_.
